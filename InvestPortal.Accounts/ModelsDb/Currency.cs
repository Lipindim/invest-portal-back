﻿using System;
using System.Collections.Generic;

namespace InvestPortal.Accounts.Models
{
    public partial class Currency
    {
        public Currency()
        {
            Account = new HashSet<Account>();
        }

        public string CurrencyId { get; set; }
        public string Name { get; set; }
        public short? DecimalPoints { get; set; }
        public string Symbol { get; set; }

        public virtual ICollection<Account> Account { get; set; }
    }
}
