﻿using System;
using System.Collections.Generic;

namespace InvestPortal.Accounts.Models
{
    public partial class Account
    {
        public string AccountId { get; set; }
        public string CurrencyId { get; set; }
        public string UserId { get; set; }
        public decimal? Balance { get; set; }

        public virtual Currency Currency { get; set; }
    }
}
