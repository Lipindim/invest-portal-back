﻿using InvestPortal.Accounts.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace InvestPortal.Accounts
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddAccountServices(this IServiceCollection serviceCollection, Action<DbContextOptionsBuilder> optionsBuilder = null)
        {
            serviceCollection.AddDbContext<AccountContext>(optionsBuilder);
            serviceCollection.AddScoped<IAccountService, AccountService>();
            return serviceCollection;
        }
    }
}
