﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using InvestPortal.Accounts.Models;

namespace InvestPortal.Accounts.Context
{
    public partial class AccountContext : DbContext
    {
        public AccountContext()
        {
        }

        public AccountContext(DbContextOptions<AccountContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Currency> Currency { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("server=localhost;user id=postgres;password=Lbvforever19;database=invest_portal;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.ToTable("account", "account");

                entity.Property(e => e.AccountId)
                    .HasColumnName("account_id")
                    .HasMaxLength(36);

                entity.Property(e => e.Balance)
                    .HasColumnName("balance")
                    .HasColumnType("numeric");

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("currency_id")
                    .HasMaxLength(10);

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasMaxLength(36);

                entity.HasOne(d => d.Currency)
                    .WithMany(p => p.Account)
                    .HasForeignKey(d => d.CurrencyId)
                    .HasConstraintName("account_fk");
            });

            modelBuilder.Entity<Currency>(entity =>
            {
                entity.ToTable("currency", "account");

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("currency_id")
                    .HasMaxLength(10);

                entity.Property(e => e.DecimalPoints).HasColumnName("decimal_points");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.Property(e => e.Symbol)
                    .HasColumnName("symbol")
                    .HasColumnType("character varying");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
