﻿using InvestPortal.Accounts.Context;
using InvestPortal.Accounts.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvestPortal.Accounts
{
    public class AccountService : IAccountService
    {
        private readonly AccountContext accountContext;

        public AccountService(AccountContext accountContext)
        {
            this.accountContext = accountContext;
            this.accountContext.Currency.Load();
        }
        public async Task<AccountInfo> CreateAccountAsync(string userId, CreateAccountModel createAccountModel)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentNullException(nameof(userId));
            if (createAccountModel == null)
                throw new ArgumentNullException(nameof(createAccountModel));

            Account account = new Account()
            {
                AccountId = Guid.NewGuid().ToString(),
                Balance = 0,
                CurrencyId = createAccountModel.CurrencyId,
                UserId = userId
            };
            var entry = this.accountContext.Account.Add(account);
            await this.accountContext.SaveChangesAsync();
            var result = new AccountInfo(account);
            entry.State = EntityState.Detached;
            return result;
        }

        public async Task DeleteAccountAsync(string userId, string accountId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentNullException(nameof(userId));
            if (string.IsNullOrWhiteSpace(accountId))
                throw new ArgumentNullException(nameof(accountId));

            Account account = await this.accountContext.Account.FirstOrDefaultAsync(x => x.AccountId == accountId);
            if (account is null)
                throw new ArgumentException($"Аккаунт '{accountId}' не существует.");
            if (account.UserId != userId)
                throw new UnauthorizedAccessException($"Вы не имеете доступа к аккаунту '{accountId}'.");

            this.accountContext.Remove(account);
            await this.accountContext.SaveChangesAsync();
        }

        public IQueryable<AccountInfo> GetUserAccounts(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentNullException(nameof(userId));

            var query = this.accountContext.Account
                .Where(a => a.UserId == userId);

            return query.Select(a => new AccountInfo(a));
        }
    }
}
