﻿using InvestPortal.Accounts.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InvestPortal.Accounts
{
    public class AccountInfo
    {
        public string AccountId { get; set; }
        public string CurrencyId { get; set; }
        public decimal? Balance { get; set; }
        public string Symbol { get; set; }
        public short? DecimalPoints { get; set; }

        public AccountInfo(Account account)
        {
            this.AccountId = account.AccountId;
            this.Balance = account.Balance;
            this.CurrencyId = account.CurrencyId;
            this.Symbol = account.Currency.Symbol;
            this.DecimalPoints = account.Currency.DecimalPoints;
        }
    }
}
