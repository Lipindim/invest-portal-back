﻿using InvestPortal.Accounts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvestPortal.Accounts
{
    public interface IAccountService
    {
        IQueryable<AccountInfo> GetUserAccounts(string userId);
        Task<AccountInfo> CreateAccountAsync(string userId, CreateAccountModel createAccountModel);
        Task DeleteAccountAsync(string userId, string accountId);
    }
}
