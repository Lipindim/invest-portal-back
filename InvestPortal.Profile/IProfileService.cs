﻿using InvestPortal.Profiles.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InvestPortal.Profiles
{
    public interface IProfileService
    {
        public Task<Profile> GetProfileAsync(string userId);
        public Task<Profile> CreateProfileAsync(string userId, CreateProfileModel createProfileModel);
        public Task<Profile> UpdateProfileAsync(string userId, UpdateProfileModel profileUpdateModel);
        public Task DeleteProfileAsync(string userId);
    }
}
