﻿using System;
using System.Collections.Generic;

namespace InvestPortal.Profiles.Models
{
    public partial class Profile
    {
        public string UserId { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Status { get; set; }
    }
}
