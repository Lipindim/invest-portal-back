﻿using InvestPortal.Profiles.Context;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace InvestPortal.Profiles
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddProfileServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddDbContext<ProfileContext>();
            serviceCollection.AddScoped<IProfileService, ProfileService>();
            return serviceCollection;
        }
    }
}
