﻿using InvestPortal.Profiles.Context;
using InvestPortal.Profiles.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvestPortal.Profiles
{
    public class ProfileService : IProfileService
    {
        private ProfileContext profileContext;

        public ProfileService(ProfileContext profileContext)
        {
            this.profileContext = profileContext;
        }
        public async Task<Profile> CreateProfileAsync(string userId, CreateProfileModel createProfileModel)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentNullException(nameof(userId));
            if (createProfileModel == null)
                throw new ArgumentNullException(nameof(createProfileModel));

            Profile profile = new Profile()
            {
                UserId = userId,
                City = createProfileModel.City,
                Email = createProfileModel.Email,
                Name = createProfileModel.Name,
                Status = createProfileModel.Status
            };
            var result = await this.profileContext.Profile.AddAsync(profile);
            await this.profileContext.SaveChangesAsync();
            result.State = Microsoft.EntityFrameworkCore.EntityState.Detached;
            return profile;
        }

        public async Task DeleteProfileAsync(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentNullException(nameof(userId));
            
            var profile = new Profile() 
            { 
                UserId = userId 
            };
            this.profileContext.Profile.Remove(profile);
            await this.profileContext.SaveChangesAsync();
        }

        public async Task<Profile> GetProfileAsync(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentNullException(nameof(userId));

            Profile profile = await this.profileContext.Profile.AsNoTracking().FirstOrDefaultAsync(p => p.UserId == userId);
            if (profile == null)
                throw new ArgumentException($"Профиль для пользователя '{userId}' не найден.", nameof(userId));
            return profile;
        }

        public async Task<Profile> UpdateProfileAsync(string userId, UpdateProfileModel profileUpdateModel)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentNullException(nameof(userId));
            if (profileUpdateModel == null)
                throw new ArgumentNullException(nameof(profileUpdateModel));

            Profile profile = new Profile()
            {
                UserId = userId,
                City = profileUpdateModel.City,
                Email = profileUpdateModel.Email,
                Name = profileUpdateModel.Name,
                Status = profileUpdateModel.Status
            };
            var result = this.profileContext.Profile.Update(profile);
            await this.profileContext.SaveChangesAsync();
            result.State = EntityState.Detached;
            return profile;
        }
    }
}
