﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InvestPortal.Profiles.Models
{
    public class BaseProfileModel
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Status { get; set; }
    }
}
