﻿using InvestPortal.Extensions;
using InvestPortal.Profiles;
using InvestPortal.Profiles.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InvestPortal.Controllers
{
    [ApiController]
    [Route("api/v1/profile")]
    public class ProfileController : ControllerBase
    {
        private readonly IProfileService profileService;

        public ProfileController(IProfileService profileService)
        {
            this.profileService = profileService;
        }

        /// <summary>
        /// Получить профиль пользователя.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<Profile> GetProfileAsync()
        {
            string userId = this.User.GetUserId();
            return await profileService.GetProfileAsync(userId);
        }

        /// <summary>
        /// Создать профиль пользователю.
        /// </summary>
        /// <param name="createProfileModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Profile> CreateProfileAsync(CreateProfileModel createProfileModel)
        {
            string userId = this.User.GetUserId();
            return await profileService.CreateProfileAsync(userId, createProfileModel);
        }

        /// <summary>
        /// Обновить профиль пользователя.
        /// </summary>
        /// <param name="updateProfileModel"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<Profile> UpdateProfileAsync(UpdateProfileModel updateProfileModel)
        {
            string userId = this.User.GetUserId();
            return await profileService.UpdateProfileAsync(userId, updateProfileModel);
        }

        /// <summary>
        /// Удалить профиль пользователя.
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task DeleteProfileAsync()
        {
            string userId = this.User.GetUserId();
            await profileService.DeleteProfileAsync(userId);
        }
    }
}
