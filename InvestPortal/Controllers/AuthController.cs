﻿using InvestPortal.Auth;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace InvestPortal.Controllers
{
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<IdentityUser> userManager;

        public AuthController(UserManager<IdentityUser> userManager)
        {
            this.userManager = userManager;
        }

        /// <summary>
        /// Регистрация пользователя.
        /// </summary>
        /// <param name="registerModel"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/auth/register")]
        public async Task<IdentityResult> RegisterUserAsync(RegisterModel registerModel)
        {
            IdentityUser identityUser = new IdentityUser(registerModel.Email)
            {
                Email = registerModel.Email
            };
            var result = await userManager.CreateAsync(identityUser, registerModel.Password).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// Аутентификация пользователя.
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        [HttpPost("/api/v1/auth/login")]
        public async Task<ActionResult> LoginAsync(LoginModel loginModel)
        {
            var identityUser = await userManager.FindByEmailAsync(loginModel.Email).ConfigureAwait(false);
            if (identityUser == null)
                throw new ArgumentException("Login failed");
            var result = userManager.PasswordHasher.VerifyHashedPassword(identityUser, identityUser.PasswordHash, loginModel.Password);
            if (result == PasswordVerificationResult.Failed)
                throw new ArgumentException("Login failed");
            var claims = new List<Claim> { new Claim(ClaimTypes.Email, identityUser.Email), new Claim(ClaimTypes.NameIdentifier, identityUser.Id) };
            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity)).ConfigureAwait(false);
            return Ok(new { Message = "You are logged in" });
        }

        /// <summary>
        /// Получение статуса пользователя: аутентифицирован он или нет.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("/api/v1/auth/me")]
        public async Task<ActionResult> AuthMeAsync()
        {
            if (this.User.Identity.IsAuthenticated)
                return this.Ok(new { IsAuth = true });
            else
                return this.Ok(new { IsAuth = false });
        }

        /// <summary>
        /// Очистить аутентификационные данные пользователя.
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/v1/auth/logout")]
        public async Task<ActionResult> LogoutAsync()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme); 
            return Ok(new { Message = "You are logged out" });
        }

    }
}
