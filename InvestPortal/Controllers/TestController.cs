﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InvestPortal.Controllers
{
    [ApiController]
    public class TestController: ControllerBase
    {
        [HttpGet("api/v1/test")]
        public ActionResult Test()
        {
            return this.Ok(new { Result = "Ok" });
        }
    }
}
