﻿using InvestPortal.Accounts;
using InvestPortal.Accounts.Models;
using InvestPortal.Extensions;
using InvestPortal.HTTP;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace InvestPortal.Controllers
{
    [ApiController]
    [Route("api/v1/accounts")]
    public class AccountsController : ControllerBase
    {
        private readonly IAccountService accountService;

        public AccountsController(IAccountService accountService)
        {
            this.accountService = accountService;
        }

        /// <summary>
        /// Получить аккаунты пользователя.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="perPage"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PaginationResponse<AccountInfo>> GetAccountsAsync(int page = 1, int perPage = 10)
        {
            string userId = this.User.GetUserId();
            var accounts = this.accountService.GetUserAccounts(userId);
            return await PaginationResponse<AccountInfo>.CreatePaginationResponse(accounts, perPage, page);
        }

        /// <summary>
        /// Создать аккаунт пользователю.
        /// </summary>
        /// <param name="createAccountModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<AccountInfo> CreateAccountAsync(CreateAccountModel createAccountModel)
        {
            string userId = this.User.GetUserId();
            var account = await this.accountService.CreateAccountAsync(userId, createAccountModel);
            return account;
        }

        /// <summary>
        /// Удалить аккаунт пользователя.
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        [HttpDelete("{accountId}")]
        public async Task DeleteAccountAsync(string accountId)
        {
            string userId = this.User.GetUserId();
            await this.accountService.DeleteAccountAsync(userId, accountId);
        }
    }
}
