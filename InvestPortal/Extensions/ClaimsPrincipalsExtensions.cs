﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace InvestPortal.Extensions
{
    public static class ClaimsPrincipalsExtensions
    {
        public static string GetUserId(this ClaimsPrincipal claimsPrincipal)
        {
            Claim idClaim = claimsPrincipal.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);
            if (idClaim == null)
                throw new UnauthorizedAccessException("Попытка неавторизированного доступа.");
            return idClaim.Value;
        }
    }
}
