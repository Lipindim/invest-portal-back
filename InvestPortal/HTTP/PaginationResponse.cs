﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;

namespace InvestPortal.HTTP
{
    public class PaginationResponse<T>
    {
        public int PerPage { get; set; }
        public int Page { get; set; }
        public int TotalPages { get; set; }
        public IEnumerable<T> Items { get; set; }

        //public PaginationResponse(IQueryable<T> data, int perPage, int page)
        //{
        //    this.Items = data.Skip(perPage * (page - 1))
        //        .Take(perPage);
        //    this.Page = page;
        //    this.PerPage = perPage;

        //    int totalCount = data.Count();
        //    this.TotalPages = (int)Math.Ceiling(((double)totalCount) / perPage);
        //}

        public async static Task<PaginationResponse<T>> CreatePaginationResponse<T>(IQueryable<T> data, int perPage, int page)
        {
            var response = new PaginationResponse<T>()
            {

                Items = await data.Skip(perPage * (page - 1)).Take(perPage).ToListAsync(),
                Page = page,
                PerPage = perPage
            };

            int totalCount = await data.CountAsync();
            response.TotalPages = (int)Math.Ceiling(((double)totalCount) / perPage);
            return response;
        }
    }
}
