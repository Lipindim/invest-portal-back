﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InvestPortal
{
    public class ExceptionFilter : IActionFilter, IOrderedFilter
    {
        private const string DetectedProblem = "We detected a problem";

        /// <summary>
        /// Initializes a new instance of <see cref="ExceptionFilter"/>.
        /// </summary>
        public ExceptionFilter()
        {
        }

        /// <summary>
        /// Gets the order value for determining the order of execution of filters.
        /// Filters execute in ascending numeric value of the <see cref="IOrderedFilter.Order"/> property.
        /// </summary>
        public int Order { get; set; } = int.MaxValue - 10;

        /// <summary>
        /// Called after the action executes, before the action result.
        /// </summary>
        /// <param name="context">The <see cref="ActionExecutedContext"/>.</param>
        public void OnActionExecuting(ActionExecutingContext context) { }

        /// <summary>
        /// Called before the action executes, after model binding is complete.
        /// </summary>
        /// <param name="context">The <see cref="ActionExecutedContext"/>.</param>
        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context?.Exception is Exception exception)
            {
                BaseResponseError resultResponseError;
                switch (exception)
                {
                    default:
                        {
                            resultResponseError = new BaseResponseError()
                            {
                                Code = -1,
                                Message = DetectedProblem,
                                Description = exception.Message,
                                StackTrace = exception.StackTrace
                            };
                        }
                        break;
                }

                context.Result ??= new BadRequestObjectResult(resultResponseError);
                context.ExceptionHandled = true;
            }
        }
    }
    internal class BaseResponseError
    {
        public BaseResponseError()
        {
        }

        public int Code { get; set; }
        public string Message { get; set; }
        public string Description { get; set; }
        public string StackTrace { get; set; }
    }
}
